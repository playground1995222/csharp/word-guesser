using word_guesser;

public class RoundEvaluation
{
    public bool correctGuess {get; private set;}
    public int numberOfMatchingLetters {get; private set;}
    public int numberOfMatchingLettersInCorrectPosition {get; private set;}
    public int numberOfGuessesLeft {get; private set;}
    public DifficultySetting difficultySetting { get; private set; }
    public int score {get; private set;}

    public RoundEvaluation(
        bool correctGuess,
        int numberOfMatchingLetters,
        int numberOfMatchingLettersInCorrectPosition,
        int numberOfGuessesLeft,
        DifficultySetting difficultySetting
        )
    {
        this.correctGuess = correctGuess;
        this.numberOfMatchingLetters = numberOfMatchingLetters;
        this.numberOfMatchingLettersInCorrectPosition = numberOfMatchingLettersInCorrectPosition;
        this.numberOfGuessesLeft = numberOfGuessesLeft;
        this.difficultySetting = difficultySetting;
        this.score = correctGuess ? ScoreCorrectGuess() : ScoreNumberOfMatchingLetters();
    }

    private int ScoreCorrectGuess()
    {
        return (difficultySetting.numberOfGuesses - numberOfGuessesLeft) * (int)Math.Pow(difficultySetting.pointsModifyier, 2);
    }

    private int ScoreNumberOfMatchingLetters()
    {
        return numberOfMatchingLetters * difficultySetting.pointsModifyier;
    }
}