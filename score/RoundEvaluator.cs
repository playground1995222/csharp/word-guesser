using word_guesser;

public class RoundEvaluator
{
    public readonly DifficultySetting difficultySetting;
    int availableNumberOfGuesses;
    public string secretWord;

    public RoundEvaluator(DifficultySetting difficultySetting, string secretWord)
    {
        this.difficultySetting = difficultySetting;
        this.secretWord = secretWord;
        availableNumberOfGuesses = difficultySetting.numberOfGuesses;
    }

    public RoundEvaluation Evaluate(string guess)
    {
        return new RoundEvaluation(
            guess == secretWord,
            GetNumberOfMatchingLetters(guess),
            GetNumberOfLettersInCorrectPosition(guess),
            availableNumberOfGuesses,
            difficultySetting
        );
    }

    private int GetNumberOfMatchingLetters(string guess)
    {
        char[] secretWordArray = secretWord.ToCharArray();
        char[] guessArray = guess.ToLower().ToCharArray();

        return secretWordArray.Intersect(guessArray).Count();
    }

    private int GetNumberOfLettersInCorrectPosition(string guess)
    {
        char[] guessArray = guess.ToLower().ToCharArray();
        return guessArray
                .Where(letter => secretWord.Contains(letter))
                .Where(letter => secretWord.IndexOf(letter) == Array.IndexOf(guessArray, letter))
                .Count();
    }

    private bool CheckIfGameOver(string guess)
    {
        return guess == secretWord;
    }

    private int ScoreCorrectGuess()
    {
        return (difficultySetting.numberOfGuesses - availableNumberOfGuesses) * (int)Math.Pow(difficultySetting.pointsModifyier, 2);
    }
}