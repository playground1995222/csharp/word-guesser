using word_guesser;

class Guesser
{
    int totalScore;
    RoundEvaluator roundEvaluator;

    public Guesser(RoundEvaluator roundEvaluator)
    {
        totalScore = 0;
        this.roundEvaluator = roundEvaluator;
    }

    public void Play()
    {
        string? wordInput;
        bool canKeepGaming = true;
        List<RoundEvaluation> evaluations = [];

        ConsolePrinter.ClearAndPrintBodyWithHeader($"You got {roundEvaluator.difficultySetting.numberOfGuesses}! Guess the word ({roundEvaluator.secretWord}):");
        do
        {
            wordInput = System.Console.ReadLine();

            if (String.IsNullOrWhiteSpace(wordInput))
            {
                throw new Exception(":/");
            }
            evaluations.Add(roundEvaluator.Evaluate(wordInput));

            canKeepGaming = evaluations.Last().correctGuess || evaluations.Last().numberOfGuessesLeft == 0;
            totalScore += evaluations.Last().score;

            ConsolePrinter.Prompt(evaluations.Last(),wordInput,totalScore);
        } while (canKeepGaming);
        string resultingPromptMessage = String.Format("End of the game! Your guess was {0} and it was {1}", wordInput, evaluations.Last().numberOfGuessesLeft >= 0 ? "correct!" : "wrong...");
        ConsolePrinter.ClearAndPrintBodyWithHeader(resultingPromptMessage);
    }
}