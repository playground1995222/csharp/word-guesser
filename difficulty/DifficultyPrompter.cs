using word_guesser;

class DifficultyPrompter
{
    DifficultyLevelMapper difficultyLevelMapper { get; set;}
    Dictionary<DifficultyLevel, DifficultySetting> difficulties {get; set;}

    public DifficultyPrompter(
        DifficultyLevelMapper difficultyLevelMapper,
        Dictionary<DifficultyLevel, DifficultySetting> difficulties)
    {
        this.difficultyLevelMapper = difficultyLevelMapper;
        this.difficulties = difficulties;
    }

    public DifficultySetting GetDifficultySettings()
    {
        DifficultyLevel difficultyLevel = DifficultyLevel.Unknown;
        while (difficultyLevel == DifficultyLevel.Unknown)
        {
            difficultyLevel = PromptForDifficultyLevel();
        }

        return difficulties[difficultyLevel];
    }

    private DifficultyLevel PromptForDifficultyLevel()
    {
        ConsolePrinter.ClearAndPrintBodyWithHeader("Choose difficulty (1: ease, 2: normal, 3: difficult)");
        string? difficultyLevelInput = System.Console.ReadLine();
        if (int.TryParse(difficultyLevelInput, out int parsedInput))
        {
            return DifficultyLevelMapper.Map(parsedInput);
        }
        return DifficultyLevel.Unknown;
    }
}