namespace word_guesser;

enum DifficultyLevel
{
    Easy, Normal, Difficult, Unknown
}