namespace word_guesser;

public record DifficultySetting(
    int numberOfGuesses,
    int pointsModifyier,
    int wordLengthUpperBound,
    int wordLengthLowerBound
);