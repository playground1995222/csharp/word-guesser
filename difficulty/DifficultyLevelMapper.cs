namespace word_guesser;

class DifficultyLevelMapper {
    public static DifficultyLevel Map(int input)
    {
        return input switch
        {
            1 => DifficultyLevel.Easy,
            2 => DifficultyLevel.Normal,
            3 => DifficultyLevel.Difficult,
            _ => DifficultyLevel.Unknown
        };
    }
}