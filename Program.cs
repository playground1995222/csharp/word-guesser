﻿namespace word_guesser;

class Program
{
    static void Main(string[] args)
    {
        RandomWordClient randomWordClient = new();
        RandomWordService randomWordService = new(randomWordClient);

        DifficultyPrompter difficultyPrompter = new(new DifficultyLevelMapper(),initDifficulties());
        DifficultySetting difficultySetting = difficultyPrompter.GetDifficultySettings();

        string word = randomWordService.GetRandomWord(difficultySetting.wordLengthLowerBound,difficultySetting.wordLengthUpperBound);
        
        RoundEvaluator roundEvaluator = new(difficultySetting,word);
        Guesser guesser = new(roundEvaluator);

        guesser.Play();
    }

    private static Dictionary<DifficultyLevel, DifficultySetting> initDifficulties()
    {
        return new Dictionary<DifficultyLevel, DifficultySetting>{
            {DifficultyLevel.Easy,new DifficultySetting(50,1,5,3)},
            {DifficultyLevel.Normal,new DifficultySetting(25,5,10,5)},
            {DifficultyLevel.Difficult,new DifficultySetting(10,10,15,10)}
        };
    }
}
