namespace word_guesser;

public class RandomWordService
{
    private readonly RandomWordClient randomWordClient;

    public RandomWordService(RandomWordClient randomWordClient)
    {
        this.randomWordClient = randomWordClient;
    }

    public string GetRandomWord(int wordLengthLowerBound, int wordLengthUpperBound)
    {
        Random random = new();
        int wordLength = random.Next(wordLengthLowerBound, wordLengthUpperBound + 1);

        List<string> words = randomWordClient.GetWord(wordLength);
        return words.Count != 0 ? words[0] : "sushi";
        // try
        // {
        //     string response = await randomWordClient.GetWord(wordLength);
        //     return response;
        // }
        // catch(HttpRequestException e)
        // {
        //     return $"Something went wrong: {e}";
        // }
    }
}