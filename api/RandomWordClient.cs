using System.Text.Json;

namespace word_guesser;

public class RandomWordClient
{
    readonly string Path = "https://random-word-api.herokuapp.com/word";

    public List<string> GetWord(int wordLength)
    {
        using (HttpClient client = new HttpClient())
        {
            HttpResponseMessage responseMessage = client.GetAsync($"{Path}?length={wordLength}").Result;
            if (responseMessage.IsSuccessStatusCode)
            {
                string jsonResponse = responseMessage.Content.ReadAsStringAsync().Result;
                return JsonSerializer.Deserialize<List<string>>(jsonResponse) ?? [];    // Coalesce operator
            }
            return [];
        }
    }
}