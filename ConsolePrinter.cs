public static class ConsolePrinter
{
  static string header = @"
         _    _               _   _____                               
        | |  | |             | | |  __ \                              
        | |  | | ___  _ __ __| | | |  \/_   _  ___  ___ ___  ___ _ __ 
        | |/\| |/ _ \| '__/ _` | | | __| | | |/ _ \/ __/ __|/ _ \ '__|
        \  /\  / (_) | | | (_| | | |_\ \ |_| |  __/\__ \__ \  __/ |   
          \/ \/ \___/|_|  \__,_|  \____/\__,_|\___||___/___/\___|_|   
  ";

  public static void ClearAndPrintBodyWithHeader(string body = "")
  {
    Console.Clear();
    Console.ForegroundColor = ConsoleColor.DarkYellow;
    Console.WriteLine(header);
    Console.ResetColor();

    if (!string.IsNullOrWhiteSpace(body))
    {
      Console.WriteLine($"\t     {body}");
    }
  }

  public static void Prompt(RoundEvaluation roundEvaluation, string wordInput, int totalScore)
  {
    ClearAndPrintBodyWithHeader();
    Console.WriteLine(@"                                         
            Your guess {0} was {1}
            It earned you {2} points (giving you a total score of {3})!
            ", wordInput, roundEvaluation.correctGuess ? "correct!" : "not correct...", roundEvaluation.score, totalScore);

    if (!roundEvaluation.correctGuess)
    {
      Console.WriteLine(@"

            You still have {0} guesses left.
            (Hint! Your last guess had {1} correct letters where {2} 
                   were in the correct position)
            ", totalScore, roundEvaluation.numberOfGuessesLeft, roundEvaluation.numberOfMatchingLetters);
    }

  }
}